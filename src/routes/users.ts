import express from "express";
import {
  getImageUploadUrl,
  getUsers,
  putImage,
  resetPassword,
  sendPasswordResetLink,
  signUp,
  verifyEmail,
} from "../controller/users";
import authGuard from "../middleware/authGuard";

const router = express.Router();

/* GET users listing. */
router.get("/", getUsers);

/** POST user signup */
router.post("/", signUp);

router.put("/password-reset-link", sendPasswordResetLink);

/** PUT user reset password */
router.put("/password", resetPassword);

/** GET user email verification */
router.get("/verification/email", verifyEmail);

/** GET image upload to s3 url */
router.get("/profile/image-url", getImageUploadUrl);

/** PUT image key */
router.put("/profile/image/:id", authGuard, putImage);

export default router;
